<div align="center">
  <img width="72" src="https://ae01.alicdn.com/kf/H25ff04ea30db4ad0abbcdd88ed4d84dbl.png"/>
  <h1>随源</h1>
  <p>🎉<code>极简</code> + <code>优雅</code>🎉</p>
</div>

---

网络导入链接（**_阅读 3.0 书源_**）：

```html
https://gitee.com/ecruos/oo/raw/master/yuedu/bookSource.json
```

---

<div align="center">
  <h2>📚 收录源 · 一览</h2>
</div>

- **优书**

  - 换源时显示小说评分
  - 正文是小说评论

- **书单**

  - 优书书单
  - 支持搜索书名、书单名
  - 支持直接搜索正文内的书单 ID

- **知轩**

  - 知轩藏书，精校小说

- **起点**

- **纵横**

- **思路客**、**九桃**、**斋书苑**、**小说旗**、**笔趣宝**等二十多个优质源

---

<div align="center">
  <h2>📋 最近更新</h2>
</div>

### 2020/04/01（v20.4.1）

- 修复`纵横`发现页
- 修复显示重复的分类
- 修复`斋书苑`正文

### 2020/03/31（v20.3.31）

- 随源的开始

---

<div align="center">
  <h2>🖼️ 效果 · 一览</h2>
</div>

> 如果看不到图片，说明你的浏览器拦截了阿里的图片（`ae01.alicdn.com`）

![换源](https://p.pstatp.com/origin/fe540000fec85e35f4f5)

---

![书单](https://ae01.alicdn.com/kf/Hc54dec351c294662abca6ad8de7d382dy.jpg)

---

![书单id](https://ae01.alicdn.com/kf/H374a81a347c14818a3e77eb062f3e99dM.jpg)
